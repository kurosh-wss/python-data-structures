class Stack():
    ''' Implementing the stack data structure

        How does stack work:
            The first element that comes in the list, can be accessed at last (FILO)
    '''

    def __init__(self):
        self.items = []
    
    def is_empty(self):
        return self.items
    
    def push(self, element):
        self.items.append(element)

    def pop(self):
        return self.items.pop()

    def peek(self):
        return self.items[-1]

    def size(self):
        return len(self.items)

    def multi_push(self, *elements):
        for i in elements:
            self.items.append(i)

    def multi_pop(self, number_of_elements):
        if number_of_elements < len(self.items):
            for i in range(number_of_elements):
                self.items.pop()
        else:
            raise Exception("List Index Out Of Range!")

    def to_str(self):
        return self.items
