class Que():
    ''' Implementing the que data structure

        How does que work:
            The first element that comes to the que comes out first too (FIFO)
    '''

    def __init__(self):
        self.items = []

    def is_empty(self):
        if len(self.items) is 0:
            return True
        else:
            return False

    def push(self, element):
        self.items.append(element)

    def pop(self):
        if self.is_empty() :
            raise Exception("Que Out Of Range!/n/tYou Can't Pop From Empty Que!")
        else:
            self.items.remove(self.items[0])

    def peek(self):
        return self.items[-1]

    def multi_pop(self, number_of_elements):
        if len(self.items) > number_of_elements:
            for i in range(number_of_elements):
                self.pop()
        else:
            raise Exception ("Que Out Of Range/n/t")
    
    def multi_push(self, *elements):
        for i in elements:
            self.push(i)

    def to_str(self):
        return self.items